import logging

from discord import Colour, Embed
from discord.ext.commands import Bot, Context, group

from bot.constants import URLs

log = logging.getLogger(__name__)


class Git:
    """Commands for linking to the source code for site and server tools."""

    def __init__(self, bot: Bot):
        self.bot = bot

    @group(name="git", aliases=("g",), invoke_without_command=True)
    async def git_group(self, ctx):
        """Commands for getting source code links."""

        await ctx.invoke(self.bot.get_command("help"), "git")

    @git_group.command(name="site")
    async def site_main(self, ctx: Context):
        """Links to source code for the server's website."""

        url = f"{URLs.gitlab_site_repo}"

        embed = Embed(title="Site Source Code")
        embed.set_footer(text=url)
        embed.colour = Colour.blurple()
        embed.description = (
            f"As our official website is an open-source community project,"
            "its source code is available [at its Gitlab Repository]({url})."
        )

        await ctx.send(embed=embed)

    @git_group.command(name="")
    async def site_resources(self, ctx: Context):
        """Links to source code for the server's bot."""

        url = f"{URLs.gitlab_bot_repo}"

        embed = Embed(title="Bot Source Code")
        embed.set_footer(text=url)
        embed.colour = Colour.blurple()
        embed.description = (
            f"As the server's bot is an open-source community project,"
            "its source code is available [at its Gitlab Repository]({url})."
        )

        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Git(bot))
    log.info("Cog loaded: Git")
